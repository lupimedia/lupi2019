<?php
$databases['default']['default'] = array (
    'database' => 'DIRECTORY_NAME',
    'username' => 'DIRECTORY_NAME',
    'password' => 'DIRECTORY_NAME',
    'prefix' => '',
    'host' => '127.0.0.1',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
);